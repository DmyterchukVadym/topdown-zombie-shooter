﻿using UnityEngine;
using UnityEditor;
using System.Reflection;

[CustomEditor(typeof(Room_Manager))]
public class Room_Manager_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Room_Manager unit = this.target as Room_Manager;
        GUILayout.Label("");
        if (GUILayout.Button("Create", EditorStyles.miniButton, GUILayout.Width(200)))
        {
            ClearLog();
            unit.CreateMainRoom();
        }
        if (GUILayout.Button("Clear", EditorStyles.miniButton, GUILayout.Width(200)))
        {
            ClearLog();
            unit.Clear();
        }
    }

    public void ClearLog() //you can copy/paste this code to the bottom of your script
    {
        var assembly = Assembly.GetAssembly(typeof(UnityEditor.Editor));
        var type = assembly.GetType("UnityEditor.LogEntries");
        var method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }
}
