﻿using UnityEngine;
using UnityEditor;
using System.Reflection;

[CustomEditor(typeof(Map_Generator))]
public class Map_Generator_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Map_Generator unit = this.target as Map_Generator;
        if (GUILayout.Button("Create", EditorStyles.miniButton, GUILayout.Width(200)))
        {
            ClearLog();
            unit.Clear();
            unit.GenerateMap();
        }
        if (GUILayout.Button("Clear", EditorStyles.miniButton, GUILayout.Width(200)))
        {
            ClearLog();
            unit.Clear();
        }

    }

    public void ClearLog() //you can copy/paste this code to the bottom of your script
    {
        var assembly = Assembly.GetAssembly(typeof(UnityEditor.Editor));
        var type = assembly.GetType("UnityEditor.LogEntries");
        var method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }
}
