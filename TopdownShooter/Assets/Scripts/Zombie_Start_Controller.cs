﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie_Start_Controller : MonoBehaviour
{
    public float speed = 0.5f;
    public float search_distance = 5;
    public int hp = 100;
    public int attackDamage;
    public float attackDelay = 1;
    [SerializeField] private Transform body;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private Sprite[] skin;
    private Rigidbody2D rb;

    private Coroutine coroutine_idle;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite.sprite = skin[Random.Range(0, skin.Length)];
        coroutine_idle = StartCoroutine(Idle());
    }

    private IEnumerator Idle()
    {
        bool idle_go = true;
        Vector3 nextPos = Vector3.zero;
        Coroutine coroutine_idle_time = null;
        bool idle_time = true;

        while (true)
        {
            if (idle_go)
            {
                float x = transform.position.x;
                float y = transform.position.y;
                nextPos = new Vector3(Random.Range(x - 3, x + 3.1f), Random.Range(y - 3, y + 3.1f), 0);
                while (Vector3.Distance(nextPos, transform.position) < 1.5)
                {
                    nextPos = new Vector3(Random.Range(x - 3, x + 3.1f), Random.Range(y - 3, y + 3.1f), 0);
                }
                idle_go = false;
            }

            if (idle_time)
            {
                coroutine_idle_time = StartCoroutine(IdleTime());
                idle_time = false;
            }

            Vector3 temp = nextPos - transform.position;
            rb.velocity = (temp) * speed;

            var angle = Vector2.Angle(Vector2.right, rb.velocity);
            if (temp.y < Vector2.right.y) angle *= -1;
            body.transform.eulerAngles = new Vector3(0f, 0f, angle);

            if (Vector3.Distance(nextPos, transform.position) < 1f)
            {
                StopCoroutine(coroutine_idle_time);
                rb.velocity = Vector3.zero;
                yield return new WaitForSeconds(Random.Range(1f, 2f));
                idle_go = true;
                idle_time = true;
            }
            yield return null;
        }
    }

    IEnumerator IdleTime()
    {
        yield return new WaitForSeconds(2);
        StopCoroutine(coroutine_idle);
        rb.velocity = Vector3.zero;
        yield return new WaitForSeconds(Random.Range(1f, 2f));
        coroutine_idle = StartCoroutine(Idle());
    }
}
