﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonFX : MonoBehaviour
{
    public AudioSource my_Fx;
    public AudioClip hover_Fx;
    public AudioClip click_Fx;

    private Canvas_controller canvas_Controller;

    public void HoverSound()
    {
        my_Fx.PlayOneShot(hover_Fx);
    }
    public void ClickSound()
    {
        my_Fx.PlayOneShot(click_Fx);
    }
    public void ChangeImageButton()
    {
        Image[] temp = GetComponentsInChildren<Image>();
        Image nowImage = GetComponent<Image>();
        for (int i = 0; i < temp.Length; i++)
        {
            if (temp[i] != nowImage) temp[i].enabled = !temp[i].enabled;
        }
    }
    public void LoadStandartGame()
    {
        canvas_Controller = FindObjectOfType<Canvas_controller>();
        canvas_Controller.InGame(Map_Generator.StartPos.Down, 6, true, 10, 10, 3, 3);
    }
    public void LoadCustomGame()
    {
        int rooms = (int)GameObject.Find("Custom_Slider").GetComponent<Slider>().value;
        bool corridor = GameObject.Find("Custom_Toggle").GetComponent<Toggle>().isOn;

        canvas_Controller = FindObjectOfType<Canvas_controller>();
        canvas_Controller.InGame(Map_Generator.StartPos.Rand, rooms, corridor, rooms + 10, rooms + 10, (rooms / 10) + 3, (rooms / 10) + 3);
    }
    public void LoadMenu()
    {
        canvas_Controller = FindObjectOfType<Canvas_controller>();
        canvas_Controller.InMenu();
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
