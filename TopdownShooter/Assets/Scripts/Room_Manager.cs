﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Linq;

public class Room_Manager : MonoBehaviour
{
    #region
    [Space(10)]
    [Header("Start")]
    [Space(10)]
    [SerializeField] private List<GameObject> startFloors;
    [SerializeField] private List<GameObject> startInteriors;

    [Space(10)]
    [Header("Room")]
    [Space(20)]
    [SerializeField] private List<GameObject> roomFloors;
    [SerializeField] private GameObject doors;
    [SerializeField] private GameObject roomWallEmptyTilemap;
    [SerializeField] private List<RuleTile> roomWallsStyles;
    [SerializeField] private List<GameObject> roomInteriors;
    [SerializeField] private GameObject roomZombieManager;

    [Space(10)]
    [Header("Corridor")]
    [Space(20)]
    [SerializeField] private List<GameObject> corridorFloors;
    [SerializeField] private List<GameObject> corridorWalls;

    [Space(10)]
    [Header("Yard")]
    [Space(20)]
    [SerializeField] private GameObject yardFloor;
    [SerializeField] private List<GameObject> yardInteriors;

    [Space(10)]
    [Header("Road")]
    [Space(20)]
    [SerializeField] private GameObject roadFloor;
    [SerializeField] private List<GameObject> roadInteriors;
    [SerializeField] private GameObject roadZombieManager;

    [Space(10)]
    [Header("Border")]
    [Space(20)]
    [SerializeField] private GameObject border;
    #endregion

    [Space(10)]
    [SerializeField] private int sid;
    private Vector3 pos;

    private int roomSize = 21;

    private int roomType;
    private int roomSettings;
    private int roomStyle;
    private int floor;
    private int interior;

    private GameObject temp;
    private string oldName = null;
    private string roomError = "RoomError";

    public int Set_SID { set { sid = value; } }

    public bool SID_Decomposition()
    {
        if (sid != 0)
        {
            roomType = sid / 100000000;
            sid -= roomType * 100000000;
            roomSettings = sid / 10000;
            sid -= roomSettings * 10000;
            roomStyle = sid / 1000;
            sid -= roomStyle * 1000;
            floor = sid / 100;
            sid -= floor * 100;
            interior = sid;
            sid -= interior;
            return true;
        }
        else
        {
            transform.name = RoomName(roomError);
            Debug.LogWarning($"{transform.name}: SID not found");
            return false;
        }
    }

    public void Clear()
    {
        Transform[] items = transform.GetComponentsInChildren<Transform>().Where(t => t.CompareTag("LayerOfRoom")).ToArray();
        if (items.Length > 0)
        {
            foreach (var item in items)
                DestroyImmediate(item.gameObject);
        }
        if (oldName != null) transform.name = oldName;
    }

    public void CreateMainRoom()
    {
        oldName = transform.name;
        pos = transform.position - new Vector3(10.5f, 10.5f, 0);
        if (SID_Decomposition())
        {
            float angle;
            switch (roomType)
            {
                case 1:
                    CreateStart();
                    switch (roomSettings)
                    {
                        case 1000:
                            angle = 180; break;
                        case 100:
                            angle = 90; break;
                        case 10:
                            angle = 0; break;
                        case 1:
                            angle = -90; break;
                        default:
                            angle = 0; break;
                    }
                    transform.Rotate(0, 0, angle);
                    return;
                case 2:
                    CreateRoom();
                    return;
                case 3:
                    CreateCorridor();
                    switch (roomSettings)
                    {
                        case 10:
                            angle = 90; break;
                        case 1:
                            angle = 0; break;
                        default:
                            angle = 0; break;
                    }
                    transform.Rotate(0, 0, angle);
                    return;
                case 4:
                    CreateYard();
                    return;
                case 5:
                    CreateRoad();
                    switch (roomSettings)
                    {
                        case 1000:
                            angle = 180; break;
                        case 100:
                            angle = 90; break;
                        case 10:
                            angle = 0; break;
                        case 1:
                            angle = -90; break;
                        default:
                            angle = 0; break;
                    }
                    transform.Rotate(0, 0, angle);
                    return;
                case 6:
                    CreateBorder();
                    return;
                default:
                    Debug.LogWarning($"{transform.name}: RoomStyle ID not found");
                    return;
            }
        }
        else
        {
            return;
        }
    }

    private void CreateBorder()
    {
        transform.name = RoomName("Border");
        temp = Instantiate(border, transform.position, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
    }

    private void CreateStart()
    {
        transform.name = RoomName("Start");
        if (floor >= startFloors.Count)
        {
            Debug.LogWarning($"{transform.name}: startFloor ID not found");
            return;
        }
        temp = Instantiate(startFloors[floor], pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
        if (interior >= startInteriors.Count)
        {
            Debug.LogWarning($"{transform.name}: startInterior ID not found");
            return;
        }
        temp = Instantiate(startInteriors[interior], pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
    }

    private void CreateRoom()
    {
        transform.name = RoomName("Room");
        if (floor >= roomFloors.Count)
        {
            Debug.LogWarning($"{transform.name}: roomFloor ID not found");
            return;
        }
        temp = Instantiate(roomFloors[floor], pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
        temp = Instantiate(doors, pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
        PaintingWalls(Instantiate(roomWallEmptyTilemap, pos, Quaternion.identity, transform));
        if (interior >= roomInteriors.Count)
        {
            Debug.LogWarning($"{transform.name}: roomInterior ID not found");
            return;
        }
        temp = Instantiate(roomInteriors[interior], pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
        temp = Instantiate(roomZombieManager, transform.position, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
    }

    private void PaintingWalls(GameObject tilemap)
    {
        if (roomStyle >= roomWallsStyles.Count)
        {
            Debug.LogWarning($"{transform.name}: roomWallsStyle ID not found");
            return;
        }
        Tilemap temp = tilemap.GetComponent<Tilemap>();
        for (int y = 0; y < roomSize; y++)
        {
            for (int x = 0; x < roomSize; x++)
            {
                if ((x == 0 || x == roomSize - 1) || (y == 0 || y == roomSize - 1))
                {
                    temp.SetTile(new Vector3Int(x, y, 0), roomWallsStyles[roomStyle]);
                    temp.tag = "LayerOfRoom";
                }
            }
        }
        int doorUp = roomSettings / 1000;
        roomSettings -= doorUp * 1000;
        int doorRight = roomSettings / 100;
        roomSettings -= doorRight * 100;
        int doorDown = roomSettings / 10;
        roomSettings -= doorDown * 10;
        int doorLeft = roomSettings;
        roomSettings -= doorLeft;
        if (doorUp != 0) temp.SetTile(new Vector3Int(roomSize / 2, roomSize - 1, 0), null);
        if (doorRight != 0) temp.SetTile(new Vector3Int(roomSize - 1, roomSize / 2, 0), null);
        if (doorDown != 0) temp.SetTile(new Vector3Int(roomSize / 2, 0, 0), null);
        if (doorLeft != 0) temp.SetTile(new Vector3Int(0, roomSize / 2, 0), null);
    }

    private void CreateCorridor()
    {
        transform.name = RoomName("Corridor");
        if (floor >= corridorFloors.Count)
        {
            Debug.LogWarning($"{transform.name}: corridorFloor ID not found");
            return;
        }
        temp = Instantiate(corridorFloors[floor], pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
        if (interior >= corridorWalls.Count)
        {
            Debug.LogWarning($"{transform.name}: corridorWall ID not found");
            return;
        }
        temp = Instantiate(corridorWalls[interior], pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
    }

    private void CreateYard()
    {
        transform.name = RoomName("Yard");
        temp = Instantiate(yardFloor, pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
        if (interior >= yardInteriors.Count)
        {
            Debug.LogWarning($"{transform.name}: yardInterior ID not found");
            return;
        }
        temp = Instantiate(yardInteriors[interior], pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
    }

    private void CreateRoad()
    {
        transform.name = RoomName("Road");
        temp = Instantiate(roadFloor, pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
        if (interior >= roadInteriors.Count)
        {
            Debug.LogWarning($"{transform.name}: roadInterior ID not found");
            return;
        }
        temp = Instantiate(roadInteriors[interior], pos, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
        temp = Instantiate(roadZombieManager, transform.position, Quaternion.identity, transform);
        temp.tag = "LayerOfRoom";
    }

    private string RoomName(string info) => $"{info} ({transform.position.x}, {transform.position.y})";
}
