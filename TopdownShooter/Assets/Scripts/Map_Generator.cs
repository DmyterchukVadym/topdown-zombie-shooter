﻿using UnityEngine;
using System.Linq;

public class Map_Generator : MonoBehaviour
{
    [SerializeField] private GameObject room_manger;
    [Space(10)]
    [Header("Map settings")]
    [Space(20)]
    [SerializeField] private StartPos startPos = StartPos.Down;
    [SerializeField] private int numberOfRooms = 6;   // кількість кімнат
    [SerializeField] private bool corridor = true;
    private int[,] rooms;   //масив, що містить 'демо-карту' розташування кімнат
    private int[,] roomsID;   //масив, що містить 'id-карту' кімнат
    private (int row, int column) start;  // позиція старту

    [Space(10)]
    [Header("Grid settings")]
    [Space(20)]
    // розміри сітки карти
    [SerializeField] private int rows = 10;  // висота
    [SerializeField] private int columns = 10;  // ширина

    [SerializeField] private int roomsOnRow = 3;   // максимальна кількість кімнат в одному рядку
    [SerializeField] private int roomsOnColumn = 3;    // максимальна кількість кімнат в одному стовбці


    [Space(10)]
    [Header("Rooms settings")]
    [Space(20)]

    // id підлоги коридору 0...9
    [SerializeField] private int startFloors = 3;
    // id інтер'єру коридору 0...99
    [SerializeField] private int startInteriors = 3;

    [Space(5)]
    // id стилю кімнати 0...9
    [SerializeField] private int roomStyles = 3;
    // id стилю підлоги 0...9
    [SerializeField] private int roomFloors = 3;
    // id інтер'єру кімнати 0...99
    [SerializeField] private int roomInteriors = 10;

    [Space(5)]
    // id підлоги коридору 0...9
    [SerializeField] private int corridorFloors = 3;
    // id інтер'єру коридору 0...99
    [SerializeField] private int corridorInteriors = 3;

    [Space(5)]
    // id інтер'єру подвір'я 0...99
    [SerializeField] private int yardInteriors = 3;

    [Space(5)]
    [SerializeField] private int roadInteriors = 3;

    [Space(20)]
    [SerializeField] private bool demoMap = false;

    //-----КОНСТАНТИ-----//
    // id типу кімнат
    private const int roomTypeStart = 1;
    private const int roomTypeRoom = 2;
    private const int roomTypeCorridor = 3;
    private const int roomTypeYard = 4;
    private const int roomTypeRoad = 5;
    private const int roomTypeBorder = 6;
    // id розташування дверей
    private const int doorTypeUp = 1000;
    private const int doorTypeRight = 100;
    private const int doorTypeDown = 10;
    private const int doorTypeLeft = 1;
    // id положення коридору
    private const int corridorTypeHorizontal = 10;
    private const int corridorTypeVertical = 1;
    // id положення(повороту) старту
    private const int startUp = 1000;
    private const int startRight = 100;
    private const int startDown = 10;
    private const int startLeft = 1;
    private int startID = startDown;

    public StartPos StartPosition { get => startPos; set => startPos = value; }
    public int NumberOfRooms { get => numberOfRooms; set => numberOfRooms = value; }
    public bool Corridor { get => corridor; set => corridor = value; }
    public int Rows { get => rows; set => rows = value; }
    public int Columns { get => columns; set => columns = value; }
    public int RoomsOnRow { get => roomsOnRow; set => roomsOnRow = value; }
    public int RoomsOnColumn { get => roomsOnColumn; set => roomsOnColumn = value; }


    public void SetStartParams(StartPos startPos, int numberOfRooms, bool corridor, int rows, int columns, int roomsOnRow, int roomsOnColumn)
    {
        this.startPos = startPos;
        this.numberOfRooms = numberOfRooms;
        this.corridor = corridor;
        this.rows = rows;
        this.columns = columns;
        this.roomsOnRow = roomsOnRow;
        this.roomsOnColumn = roomsOnColumn;
    }

    public void Clear()
    {
        Transform[] items = transform.GetComponentsInChildren<Transform>().Where(t => t.CompareTag("Room")).ToArray();
        if (items.Length > 0)
        {
            foreach (var item in items)
                DestroyImmediate(item.gameObject);
        }
    }

    public void GenerateMap()
    {
        rooms = new int[rows, columns];
        roomsID = new int[rows, columns];
        GenerateDemoMap();
        if (corridor) GenerateCorridors();
        CutEmptiness();
        if (demoMap) PrintMap();
        GenerateRoomsSID();
        CreateRoom();
    }

    public enum StartPos
    {
        Up,
        Right,
        Down,
        Left,
        Rand
    }

    // Метод, що генерує 'демо-карту' розташування кімнат (без типів кімнат)
    private void GenerateDemoMap()
    {
        int value;
        (int row, int column)[] startPoint = new (int row, int column)[] { (1, columns / 2), (rows / 2, columns - 2), (rows - 2, columns / 2), (rows / 2, 1) };

        (int row, int column) firstRoom;

        switch (startPos)
        {
            case StartPos.Up:
                value = 0;
                break;
            case StartPos.Right:
                value = 1;
                break;
            case StartPos.Down:
                value = 2;
                break;
            case StartPos.Left:
                value = 3;
                break;
            case StartPos.Rand:
                value = Random.Range(0, startPoint.Length);
                break;
            default:
                print("<StartPos>: Стартова позиція задана некоректно");
                return;
        }

        switch (value)
        {
            case 0:
                start = startPoint[value];
                startID = startUp;
                firstRoom.row = start.row + 1;
                firstRoom.column = columns / 2;
                break;
            case 1:
                start = startPoint[value];
                startID = startRight;
                firstRoom.row = rows / 2;
                firstRoom.column = start.column - 1;
                break;
            case 2:
                start = startPoint[value];
                startID = startDown;
                firstRoom.row = start.row - 1;
                firstRoom.column = columns / 2;
                break;
            case 3:
                start = startPoint[value];
                startID = startLeft;
                firstRoom.row = rows / 2;
                firstRoom.column = start.column + 1;
                break;
            default:
                print("<value>: Стартова позиція задана некоректно");
                return;
        }

        rooms[start.row, start.column] = 9;
        rooms[firstRoom.row, firstRoom.column] = 1;

        int count = 1;
        int exitCount = 0;
        while (count < numberOfRooms)
        {
            (int i, int j) pos = (Random.Range(2, rows - 2), Random.Range(2, columns - 2));
            if ((!Check(pos, Level.firstLevel) ^ !corridor) && (Check(pos, Level.secondLevel) | !corridor) && CheckRow(pos.i) && CheckColumn(pos.j))
            {
                rooms[pos.i, pos.j] = 1;
                count++;
            }
            exitCount++;
            if (exitCount > 100000)
            {
                Debug.LogWarning("Неможливо створити нову кімнату. Недостатно місця");
                numberOfRooms = count;
                break;
            }
        }
    }

    // Метод перевіряє чи існує кімната (зверху, справа, знизу та зліва) відносно заданої позиції на першому (Level.firstLevel) або другому (Level.secondLevel) рівнях
    // Повертає "true", якщо існує хоча б одна кімната (зверху, справа, знизу або зліва відносно заданої позиції)
    // (int row, int column) pos - координати для перевірки (<row> - номер рядка (pos.row), <column> - номер стовбця (pos.column))
    private bool Check((int row, int column) pos, Level step)
    {
        int level;
        switch (step)
        {
            case Level.firstLevel:
                level = 1;
                break;
            case Level.secondLevel:
                level = 2;
                break;
            default:
                level = 0;
                break;
        }
        if (rooms[pos.row, pos.column] != 0) return false;
        bool up = pos.row - 1 * level >= 0;
        bool right = pos.column + 1 * level < columns;
        bool down = pos.row + 1 * level < rows;
        bool left = pos.column - 1 * level >= 0;

        return (up && rooms[pos.row - 1 * level, pos.column] != 0) ||
            (right && rooms[pos.row, pos.column + 1 * level] != 0) ||
            (down && rooms[pos.row + 1 * level, pos.column] != 0) ||
            (left && rooms[pos.row, pos.column - 1 * level] != 0);
    }

    enum Level
    {
        firstLevel,
        secondLevel
    }

    // Генератор коридорів між кімнатами
    private void GenerateCorridors()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (rooms[i, j] == 1)
                {
                    if (i - 2 >= 0) if (rooms[i - 2, j] == 1) rooms[i - 1, j] = 2;
                    if (j + 2 < columns) if (rooms[i, j + 2] == 1) rooms[i, j + 1] = 2;
                    if (i + 2 < rows) if (rooms[i + 2, j] == 1) rooms[i + 1, j] = 2;
                    if (j - 2 >= 0) if (rooms[i, j - 2] == 1) rooms[i, j - 1] = 2;
                }
            }
        }
    }

    // Генерація 'ID - карти', що містить масив з ключами для побудови кожної кімнати
    // Метод генерує  SID кімнат (на основі 'Demo-карти'), який містить інформацію про тип кімнати, розташування дверей 
    // (у випадку коридора - положення вертикальне/горизонтальне), стиль та один із варіантів інтер'єру
    private void GenerateRoomsSID()
    {

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                switch (rooms[i, j])
                {
                    case -1:
                        roomsID[i, j] = roomTypeBorder * 100000000;
                        break;
                    case 0:
                        roomsID[i, j] = roomTypeYard * 100000000 + Random.Range(0, yardInteriors);
                        break;
                    case 1:
                        roomsID[i, j] = roomTypeRoom * 100000000 + SetDoorTypeID((i, j)) * 10000 + Random.Range(0, roomStyles) * 1000 + Random.Range(0, roomFloors) * 100 + Random.Range(0, roomInteriors);
                        break;
                    case 2:
                        roomsID[i, j] = roomTypeCorridor * 100000000 + SetCorridorType((i, j)) * 10000 + Random.Range(0, corridorFloors) * 100 + Random.Range(0, corridorInteriors);
                        break;
                    case 8:
                        roomsID[i, j] = roomTypeRoad * 100000000 + startID * 10000 + Random.Range(0, roadInteriors);
                        break;
                    case 9:
                        roomsID[i, j] = roomTypeStart * 100000000 + startID * 10000 + Random.Range(0, startFloors) * 100 + Random.Range(0, startInteriors);
                        break;
                    default:
                        break;
                }
            }
        }

        switch (startID)
        {
            case 1000:
                for (int i = 0; i < columns; i++)
                    if (rooms[start.row, i] == 0)
                        roomsID[start.row, i] = roomTypeRoad * 100000000 + startID * 10000;
                break;
            case 100:
                for (int i = 0; i < rows; i++)
                    if (rooms[i, start.column] == 0)
                        roomsID[i, start.column] = roomTypeRoad * 100000000 + startID * 10000;
                break;
            case 10:
                for (int i = 0; i < columns; i++)
                    if (rooms[start.row, i] == 0)
                        roomsID[start.row, i] = roomTypeRoad * 100000000 + startID * 10000;
                break;
            case 1:
                for (int i = 0; i < rows; i++)
                    if (rooms[i, start.column] == 0)
                        roomsID[i, start.column] = roomTypeRoad * 100000000 + startID * 10000;
                break;
            default:
                break;
        }
    }

    // Визначення положення дверей в кімнаті
    private int SetDoorTypeID((int row, int column) pos)
    {
        int roomID = 0;
        if (pos.row - 1 >= 0) if (rooms[pos.row - 1, pos.column] != 0) roomID += doorTypeUp;
        if (pos.column + 1 < columns) if (rooms[pos.row, pos.column + 1] != 0) roomID += doorTypeRight;
        if (pos.row + 1 < rows) if (rooms[pos.row + 1, pos.column] != 0) roomID += doorTypeDown;
        if (pos.column - 1 >= 0) if (rooms[pos.row, pos.column - 1] != 0) roomID += doorTypeLeft;
        return roomID;
    }

    // Визначення положення коридору (горизонтальне/вертикальне)
    private int SetCorridorType((int row, int column) pos)
    {
        if ((pos.column - 1 >= 0) && (pos.column + 1 < columns))
        {
            if ((rooms[pos.row, pos.column - 1] == 1) && (rooms[pos.row, pos.column + 1] == 1)) return corridorTypeHorizontal;
            else return corridorTypeVertical;
        }
        else return corridorTypeVertical;

    }

    // Метод для перевірки умови допустимої кількості кімнат <maxNumberOfRoomOnRow> в заданому рядку <row>
    // Повертає "true", якщо кількість існуючих кімнат в рядку менша за допустиму
    private bool CheckRow(int row)
    {
        int count = 0;
        for (int j = 0; j < columns; j++)
        {
            if (rooms[row, j] == 1) count++;
        }
        return count < roomsOnRow;
    }

    // Метод для перевірки умови допустимої кількості кімнат <maxNumberOfRoomOnColumn> в заданому стовбці <column>
    // Повертає "true", якщо кількість існуючих кімнат в стовбці менша за допустиму
    private bool CheckColumn(int column)
    {
        int count = 0;
        for (int i = 0; i < rows; i++)
        {
            if (rooms[i, column] == 1) count++;
        }
        return count < roomsOnColumn;
    }

    // Вивід 'демо-карти'
    private void PrintMap()
    {
        string rez = null;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (rooms[i, j] != -2)
                    rez += rooms[i, j] + " ";
            }
            if (rez != null)
                print(rez);
            rez = null;
        }
    }

    public void CreateRoom()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (roomsID[i, j] != 0)
                {
                    GameObject temp = Instantiate(room_manger, new Vector3((j - start.column) * 21, (i - start.row) * -21, 0), Quaternion.identity, transform);
                    temp.transform.tag = "Room";
                    temp.GetComponent<Room_Manager>().Set_SID = roomsID[i, j];
                    temp.GetComponent<Room_Manager>().CreateMainRoom();
                }
            }
        }
        FindObjectOfType<Canvas_controller>().IsDone = true;
    }

    private void CutEmptiness()
    {
        int[] x = new int[numberOfRooms];
        int[] y = new int[numberOfRooms];
        int k = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (rooms[i, j] == 1)
                {
                    x[k] = j;
                    y[k] = i;
                    k++;
                }
            }
        }
        System.Array.Sort(x);
        System.Array.Sort(y);

        int x_min = x[0];
        int x_max = x[numberOfRooms - 1];
        int y_min = y[0];
        int y_max = y[numberOfRooms - 1];

        if (x_min - 0 > 2)
            for (int i = 0; i < rows; i++)
                for (int j = 0; j <= x_min - 3; j++)
                    rooms[i, j] = -2;
        if (columns - 1 - x_max > 2)
            for (int i = 0; i < rows; i++)
                for (int j = x_max + 3; j < columns; j++)
                    rooms[i, j] = -2;
        if (y_min - 0 > 2)
            for (int i = 0; i <= y_min - 3; i++)
                for (int j = 0; j < columns; j++)
                    rooms[i, j] = -2;
        if (rows - 1 - y_max > 2)
            for (int i = y_max + 3; i < rows; i++)
                for (int j = 0; j < columns; j++)
                    rooms[i, j] = -2;

        x_min -= 2;
        x_max += 3;
        y_min -= 2;
        y_max += 3;
        for (int i = y_min; i < y_max; i++)
        {
            for (int j = x_min; j < x_max; j++)
            {
                if ((i == y_min) || (i == y_max - 1) || (j == x_min) || (j == x_max - 1))
                {
                    rooms[i, j] = -1;
                }
            }
        }

        FindObjectOfType<Camera_Move>().SetBorders(((x_min + 1 - start.column) * 21 - 10.5f, (x_max - 2 - start.column) * 21 + 10.5f, (y_max - 2 - start.row) * -21 - 10.5f, (y_min + 1 - start.row) * -21 + 10.5f));
    }
}
