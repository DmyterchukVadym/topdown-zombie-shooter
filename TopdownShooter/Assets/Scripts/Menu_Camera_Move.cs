﻿using System.Collections;
using UnityEngine;

public class Menu_Camera_Move : MonoBehaviour
{
    [SerializeField] private Transform point_A;
    [SerializeField] private Transform point_B;
    public float dumping = 0.5f;
    private Vector3 _target_Point;
    private Transform _transform;
    void Start()
    {
        _transform = transform;
        _transform.position = new Vector3(0, 0, _transform.position.z);
        _target_Point = new Vector3(22, 37, _transform.position.z);
    }
    private IEnumerator NextPointForCamera()
    {
        while(Vector3.Distance(_target_Point, _transform.position) <= 50)
        {
            _target_Point = new Vector3(Random.Range(point_A.position.x, point_B.position.x),
                                        Random.Range(point_A.position.y, point_B.position.y),
                                        _transform.position.z);
        }
        yield return new WaitForSeconds(1);
    }
    private void Update()
    {
        if(Vector3.Distance(_target_Point, _transform.position) >= 1)
        {
            _transform.position = Vector3.MoveTowards(_transform.position, _target_Point, Time.deltaTime * dumping);
        }
        else
        {
            StartCoroutine(NextPointForCamera());
        }
    }
}
