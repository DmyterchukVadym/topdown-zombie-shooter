﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player_Controller : MonoBehaviour
{
    
    private Rigidbody2D player;


    [SerializeField] private float speed = 0.1f;
    [SerializeField] private int hp = 100;

    private bool move = true;

    [SerializeField] private AudioSource audioSourceWalk;
    [SerializeField] AudioClip ground;
    [SerializeField] AudioClip wood;
    [SerializeField] AudioClip tile;
    [SerializeField] AudioClip road;

    

    private VariableJoystick joystick;
    private HP_Status_Bar hp_status;
    private Transform _cursor;
    public void AddHP(int hp)
    {
        this.hp += hp;
        if (this.hp > 100) this.hp = 100;
        hp_status.SetHP(this.hp);
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Rigidbody2D>();
        audioSourceWalk.clip = ground;

        joystick = FindObjectOfType<VariableJoystick>();
        print(joystick.name);
        hp_status = FindObjectOfType<HP_Status_Bar>();
        _cursor= FindObjectOfType<Curssair_For_Shot>().GetComponent<Transform>();
        hp_status.MaxHP = hp;
        hp_status.SetHP(hp);
    }
    
    // Update is called once per frame
    void Update()
    {
        if (move) player.velocity = new Vector3(joystick.Horizontal, joystick.Vertical, 0) * speed; // if (move) player.velocity = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0) * speed;
        /*if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            var mousePosition = Camera.main.ScreenToWorldPoint(touch.position);
            var angle = Vector2.Angle(Vector2.right, mousePosition - transform.position);
            transform.eulerAngles = new Vector3(0f, 0f, transform.position.y < mousePosition.y ? angle : -angle);
        }*/
        var mousePosition = Camera.main.ScreenToWorldPoint(_cursor.position); //var mousePosition = Input.mousePosition;
        //mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        var angle = Vector2.Angle(Vector2.right, mousePosition - Camera.main.ScreenToWorldPoint(transform.position));
        transform.eulerAngles = new Vector3(0f, 0f, transform.position.y < _cursor.position.y ? angle : -angle);
        
        /*Vector3 pos = Camera.main.ScreenToWorldPoint(_cursor.position);
        pos.z = 0;
        pos = pos - transform.position;
        transform.rotation = Quaternion.FromToRotation(Vector3.up, pos);*/

        if (player.velocity.magnitude > 0.1f && !audioSourceWalk.isPlaying)
        {
            audioSourceWalk.Play();
            print(_cursor.position);
        }
        else if (player.velocity.magnitude < 0.1f && audioSourceWalk.isPlaying)
        {
            audioSourceWalk.Stop();
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Ground":
                if (audioSourceWalk.clip != ground)
                {
                    audioSourceWalk.Stop();
                    audioSourceWalk.clip = ground;
                    audioSourceWalk.Play();
                }
                break;
            case "Wood":
                if (audioSourceWalk.clip != wood)
                {
                    audioSourceWalk.Stop();
                    audioSourceWalk.clip = wood;
                    audioSourceWalk.Play();
                }
                break;
            case "Tile":
                if (audioSourceWalk.clip != tile)
                {
                    audioSourceWalk.Stop();
                    audioSourceWalk.clip = tile;
                    audioSourceWalk.Play();
                }
                break;
            case "Road":
                if (audioSourceWalk.clip != road)
                {
                    audioSourceWalk.Stop();
                    audioSourceWalk.clip = road;
                    audioSourceWalk.Play();
                }
                break;
            default:
                break;
        }
    }

    private IEnumerator GetDamage()
    {
        SpriteRenderer sprite = GetComponentInChildren<SpriteRenderer>();
        sprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        sprite.color = Color.white;
    }

    public IEnumerator Force(Vector3 force)
    {
        move = false;
        player.velocity = Vector3.zero;
        player.AddForce(force);
        yield return new WaitForSeconds(.25f);
        move = true;
    }

    public void Damage(int damage)
    {
        hp -= damage;
        hp_status.SetHP(hp);
        if (hp <= 0)
        {
            //GameObject.Find("Canvas_game_over").GetComponent<Canvas>().enabled = true;
            FindObjectOfType<Canvas_In_Game>().ActiveGameOver();
            Destroy(_cursor.gameObject);
            Destroy(gameObject);
        }
        StartCoroutine(GetDamage());
    }
}
