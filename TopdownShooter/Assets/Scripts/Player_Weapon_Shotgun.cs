﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Weapon_Shotgun : MonoBehaviour
{
    private bool isShoot = true;
    [SerializeField] private float attackDelay = 1f;
    [SerializeField] private ParticleSystem bullet;
    [SerializeField] private ParticleSystem flash;

    private AudioSource audioSourceShot;
    [SerializeField] private AudioClip shotgunShot;
    [SerializeField] private AudioClip noAmmo;

    private Player_Weapon_Manager pwm;

    private void OnEnable()
    {
        isShoot = true;
        audioSourceShot = transform.parent.gameObject.GetComponent<AudioSource>();
        pwm = transform.parent.gameObject.GetComponent<Player_Weapon_Manager>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && isShoot)
        {
            if (pwm.AmmoShotgun > 0)
            {
                bullet.Emit(20);
                flash.Emit(1);
                audioSourceShot.PlayOneShot(shotgunShot);
                pwm.AmmoShotgun = 1;
            }
            else audioSourceShot.PlayOneShot(noAmmo);
            isShoot = false;
            StartCoroutine(ShootDelay());
        }
    }

    IEnumerator ShootDelay()
    {
        yield return new WaitForSeconds(attackDelay);
        isShoot = true;
    }
}
