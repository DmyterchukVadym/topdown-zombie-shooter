using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Curssair_For_Shot : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Transform _transform;
    private Image _image;
    private Vector3 _offset;
    void Start()
    {
        _image = GetComponent<Image>();
        _transform = transform;
    }

    void IBeginDragHandler.OnBeginDrag(PointerEventData touch)
    {
        _offset = _transform.position - (Vector3)Camera.main.ScreenToWorldPoint(touch.position); 
        _offset.z = 0;

        _image.raycastTarget = false;
    }
    public void OnDrag(PointerEventData touch)
    {
        var dragPosition = (Vector3)Camera.main.ScreenToWorldPoint(touch.position);
        dragPosition.z = 0;
        _transform.position = dragPosition + _offset;
    }


    public void OnEndDrag(PointerEventData touch)
    {
        _image.raycastTarget = true;
    }
    
}
