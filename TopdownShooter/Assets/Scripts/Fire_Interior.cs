﻿using UnityEngine;

public class Fire_Interior : MonoBehaviour
{
    private SpriteRenderer _spriteRenderer;
    private Animator _animator;
    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
    }
    private void OnBecameVisible()
    {
        _spriteRenderer.enabled = true;
        _animator.enabled = true;
    }
    private void OnBecameInvisible()
    {
        _spriteRenderer.enabled = false;
        _animator.enabled = false;
    }
}
