﻿using UnityEngine;
using System;

public class Camera_Move : MonoBehaviour
{
    [SerializeField] private bool freeCamera = false;
    private Transform player;
    private bool search = true;
    public (float x_min, float x_max, float y_min, float y_max) borders;
    private (float x, float y) cameraSize;
    private Transform _transform;

    private void Start()
    {
        _transform = transform;
    }
    public void Reset() => search = true;
    public void SetBorders((float x_min, float x_max, float y_min, float y_max) borders)
    {
        this.borders.x_min = borders.x_min;
        this.borders.x_max = borders.x_max;
        this.borders.y_min = borders.y_min;
        this.borders.y_max = borders.y_max;
        SetCameraSize();
    }
    public void SetCameraSize()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        Vector2 temp = max - min;
        cameraSize.x = (float)Math.Round(temp.x / 2, 1, MidpointRounding.AwayFromZero);
        cameraSize.y = (float)Math.Round(temp.y / 2, 1, MidpointRounding.AwayFromZero);
    }
    private void OnApplicationFocus(bool focus)
    {
        SetCameraSize();
    }
    void Update()
    {
        if (search)
        {
            try
            {
                player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
                if (player)
                {
                    search = false;
                    _transform.position = new Vector3(player.position.x, player.position.y, _transform.position.z);
                }
            }
            catch
            {
                Debug.LogWarning("Player not found");
            }
        }
        else if (player)
        {
            if (freeCamera)
            {
                _transform.position = new Vector3(player.transform.position.x, _transform.position.y, _transform.position.z);
            }
            else
            {
                if ((player.transform.position.x > borders.x_min + cameraSize.x) &&
                    (player.transform.position.x < borders.x_max - cameraSize.x))
                    _transform.position = new Vector3(player.transform.position.x, _transform.position.y, _transform.position.z);
                if ((player.transform.position.y > borders.y_min + cameraSize.y) &&
                    (player.transform.position.y < borders.y_max - cameraSize.y))
                    _transform.position = new Vector3(_transform.position.x, player.transform.position.y, _transform.position.z);
            }
        }
    }
}
