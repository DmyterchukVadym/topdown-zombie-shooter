﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Menu_Zombie : MonoBehaviour
{
    [SerializeField] GameObject zombie;
    Transform[] spawnpoints;
    void Start()
    {
        spawnpoints = GetComponentsInChildren<Transform>().Where(t => t.CompareTag("SpawnPoint")).ToArray();
        for (int i = 0; i < spawnpoints.Length; i++)
        {
            if (i % 3 == 0)
                Instantiate(zombie, spawnpoints[i].position, Quaternion.identity, transform);
        }
    }
}
