﻿using UnityEngine;

public class GameSettings : MonoBehaviour
{
    public Texture2D cursorTexture;
    CursorMode cursorMode = CursorMode.Auto;
    Vector2 hotSpot = new Vector2(15, 15);
    void Start()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
}
