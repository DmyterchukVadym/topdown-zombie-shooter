﻿using UnityEngine;
using System.Collections;

public class Canvas_In_Game : MonoBehaviour
{
    [SerializeField] private GameObject panelHealsBar;
    [SerializeField] private GameObject panelWeapon;
    [SerializeField] private GameObject panelFastIcon;
    [SerializeField] private GameObject panelJoystick;


    [SerializeField] private GameObject panelMissionCompleted;
    [SerializeField] private GameObject buttonMissionCompleted;
    [SerializeField] private GameObject panelGameOver;
    [SerializeField] private GameObject buttonGameOver;

    private Music_Manager music_Manager;
    private void Start()
    {
        music_Manager = FindObjectOfType<Music_Manager>();
    }
    public void ActiveMissionCompleted()
    {
        buttonMissionCompleted.SetActive(false);
        buttonGameOver.SetActive(false);
        StartCoroutine(ActiveButtonToMenu());
        DisabledPanelInGame();
        music_Manager.MissionCompletedPlayMusic();
        panelMissionCompleted.SetActive(true);
    }
    public void ActiveGameOver()
    {
        
        buttonMissionCompleted.SetActive(false);
        buttonGameOver.SetActive(false);
        StartCoroutine(ActiveButtonToMenu());
        music_Manager.GameOverPlayMusic();
        DisabledPanelInGame();
        panelGameOver.SetActive(true);
    }
    public void EnabledPanelInGame()
    {
        panelHealsBar.SetActive(true);
        panelWeapon.SetActive(true);
        panelFastIcon.SetActive(true);
        panelJoystick.SetActive(true);
    }
    public void DisabledPanelInGame()
    {
        
        panelHealsBar.SetActive(false);
        panelWeapon.SetActive(false);
        panelFastIcon.SetActive(false);
        panelJoystick.SetActive(false);
    }
    private IEnumerator ActiveButtonToMenu()
    {
        yield return new WaitForSeconds(5);
        buttonMissionCompleted.SetActive(true);
        buttonGameOver.SetActive(true);
    }
}
