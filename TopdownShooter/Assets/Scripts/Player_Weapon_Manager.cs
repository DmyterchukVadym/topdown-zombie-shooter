﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Weapon_Manager : MonoBehaviour
{
    public bool none = true;
    public bool pistol = false;
    public bool shotgun = false;
    public bool uzi = false;
    [SerializeField] private Weapon startWeapon;
    private Weapon currentWeapon;
    [SerializeField] private GameObject[] weapons;

    [SerializeField] private int ammoShotgun = 20;
    [SerializeField] private int ammoUzi = 100;

    [SerializeField] private Sprite[] weaponSprites;
    private Image weaponImage;
    private Text countBullet;

    private Player_Controller player;

    public int AmmoShotgun { get => ammoShotgun; set { ammoShotgun -= value; ShowAmmo(); } }
    public int AmmoUzi { get => ammoUzi; set { { ammoUzi -= value; ShowAmmo(); } } }

    // Start is called before the first frame update
    // &&&&&&
    void Start()
    {
        weaponImage = GameObject.Find("Image_weapon").GetComponent<Image>();
        countBullet = GameObject.Find("Text_count_bullet").GetComponent<Text>();
        SelectWeapon(startWeapon);
        player = GetComponent<Player_Controller>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0)) SelectWeapon(Weapon.None);
        if (Input.GetKeyDown(KeyCode.Alpha1)) SelectWeapon(Weapon.Pistol);
        if (Input.GetKeyDown(KeyCode.Alpha2)) SelectWeapon(Weapon.Shotgun);
        if (Input.GetKeyDown(KeyCode.Alpha3)) SelectWeapon(Weapon.Uzi);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "ShotgunAmmo":
                ammoShotgun += 5;
                ShowAmmo();
                Destroy(collision.gameObject);
                break;
            case "UziAmmo":
                ammoUzi += 15;
                ShowAmmo();
                Destroy(collision.gameObject);
                break;
            case "FirstAidKit":
                player.AddHP(50);
                Destroy(collision.gameObject);
                break;
        }

    }

    private void SelectWeapon(Weapon weapon)
    {
        switch (weapon)
        {
            case Weapon.None:
                if (none)
                {
                    ResetWeapon();
                    weapons[0].SetActive(true);
                    currentWeapon = weapon;
                    weaponImage.sprite = weaponSprites[0];
                    ShowAmmo();
                }
                else return;
                break;
            case Weapon.Pistol:
                if (pistol)
                {
                    ResetWeapon();
                    weapons[1].SetActive(true);
                    currentWeapon = weapon;
                    weaponImage.sprite = weaponSprites[1];
                    ShowAmmo();
                }
                else return;
                break;
            case Weapon.Shotgun:
                if (shotgun)
                {
                    ResetWeapon();
                    weapons[2].SetActive(true);
                    currentWeapon = weapon;
                    weaponImage.sprite = weaponSprites[2];
                    ShowAmmo();
                }
                else return;
                break;
            case Weapon.Uzi:
                if (uzi)
                {
                    ResetWeapon();
                    weapons[3].SetActive(true);
                    currentWeapon = weapon;
                    weaponImage.sprite = weaponSprites[3];
                    ShowAmmo();
                }
                else return;
                break;
            default:
                print("Weapon selection error");
                break;
        }
    }

    private void ShowAmmo()
    {
        switch (currentWeapon)
        {
            case Weapon.None:
                countBullet.text = "";
                break;
            case Weapon.Pistol:
                countBullet.text = "\u221E";
                break;
            case Weapon.Shotgun:
                countBullet.text = ammoShotgun.ToString();
                break;
            case Weapon.Uzi:
                countBullet.text = ammoUzi.ToString();
                break;
            default:
                break;
        }
    }

    private void ResetWeapon()
    {
        foreach (var item in weapons)
        {
            item.SetActive(false);
        }
    }

    enum Weapon
    {
        None,
        Pistol,
        Shotgun,
        Uzi
    }
}
