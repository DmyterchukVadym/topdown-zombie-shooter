﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_Loading_Game : MonoBehaviour
{
    [SerializeField] private Image logoImage;
    [SerializeField] private Image backgroundImage;
    [SerializeField] private Sprite[] backgroundImages;

    public float dumping = 2f;

    public void LogoBlick()
    {
        StartCoroutine(StartLogoBlick());
    }
    private IEnumerator StartLogoBlick()
    {
        backgroundImage.sprite = backgroundImages[Random.Range(0, backgroundImages.Length)];
        yield return null;
    }
}
