﻿using UnityEngine;
using UnityEngine.Events;

public class Damage_Event : MonoBehaviour
{
    public MyDamage SetDamage;
}

[System.Serializable]
public class MyDamage : UnityEvent<int>
{

}
