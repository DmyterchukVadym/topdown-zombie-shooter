﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ParticleSystemJobs;



public class Player_Weapon_Pistol : MonoBehaviour
{
    private bool isShoot = true;
    [SerializeField] private float attackDelay = 0.5f;
    [SerializeField] private ParticleSystem bullet;
    [SerializeField] private ParticleSystem flash;

    private AudioSource audioSourceShot;
    [SerializeField] private AudioClip pistolShot;
    [SerializeField] private AudioClip noAmmo;


    private void OnEnable()
    {
        isShoot = true;
        audioSourceShot = transform.parent.gameObject.GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && isShoot)
        {
            var mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            mousePosition = new Vector3(mousePosition.x, mousePosition.y, 0);
            if (Vector3.Distance(mousePosition, bullet.transform.position) > 3)
            {
                var angle = Vector2.Angle(Vector2.up, mousePosition - bullet.transform.position);
                bullet.transform.eulerAngles = new Vector3(0f, 0f, bullet.transform.position.x > mousePosition.x ? angle : -angle);
            }
            else
            {
                bullet.transform.localEulerAngles = new Vector3(0f, 0f, -90f);
            }
            bullet.Emit(1);
            flash.Emit(1);
            audioSourceShot.PlayOneShot(pistolShot);
            isShoot = false;
            StartCoroutine(ShootDelay());
        }
    }

    IEnumerator ShootDelay()
    {
        yield return new WaitForSeconds(attackDelay);
        isShoot = true;
    }
}
