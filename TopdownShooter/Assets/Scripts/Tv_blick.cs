﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tv_blick : MonoBehaviour
{
    [SerializeField] private SpriteRenderer ligth;
    private void OnBecameVisible()
    {
        StartCoroutine(Blick());
    }
    private void OnBecameInvisible()
    {
        StopCoroutine(Blick());
    }
    private IEnumerator Blick()
    {
        while (true)
        {
            ligth.enabled = false;
            yield return new WaitForSeconds(Random.Range(0.1f, 1f));
            ligth.enabled = true;
            yield return new WaitForSeconds(Random.Range(0.1f, 1f));
        }
    }
}
