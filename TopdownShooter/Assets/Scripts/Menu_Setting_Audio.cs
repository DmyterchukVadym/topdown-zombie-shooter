﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Menu_Setting_Audio : MonoBehaviour
{
    public AudioMixerGroup mixer;
    public float masterVolumeValue;
    public Text textMasterVolume;
    public Text textSoundVolume;
    public Text textMusicVolume;

    [SerializeField] private Slider master;
    [SerializeField] private Slider music;
    [SerializeField] private Slider sound;


    private float musicVolumeValue;
    private float soundVolumeValue;

    private void Start()
    {
        GetVolume();
        SetSliderValue();
    }
    
    public void ChangeMasterVolume(float volume)
    {
        mixer.audioMixer.SetFloat("MasterVolume", Mathf.Lerp(-80, 0, volume));
        textMasterVolume.text = Mathf.RoundToInt(volume * 100).ToString();
        mixer.audioMixer.GetFloat("MasterVolume", out masterVolumeValue);
    }
    public void ChangeMusicVolume(float volume)
    {
        mixer.audioMixer.SetFloat("MusicVolume", Mathf.Lerp(-80, 0, volume));
        textMusicVolume.text = Mathf.RoundToInt(volume * 100).ToString();
    }
    public void ChangeSoundsVolume(float volume)
    {
        mixer.audioMixer.SetFloat("SoundVolume", Mathf.Lerp(-80, 0, volume));
        textSoundVolume.text = Mathf.RoundToInt(volume * 100).ToString();
    }
    public void GetVolume()
    {
        mixer.audioMixer.GetFloat("MasterVolume", out masterVolumeValue);
        masterVolumeValue = (masterVolumeValue + 80) / 80;
        mixer.audioMixer.GetFloat("MusicVolume", out musicVolumeValue);
        musicVolumeValue = (musicVolumeValue + 80) / 80;
        mixer.audioMixer.GetFloat("MasterVolume", out soundVolumeValue);
        soundVolumeValue = (soundVolumeValue + 80) / 80;
    }
    public void SetVolume()
    {
        mixer.audioMixer.SetFloat("MasterVolume", Mathf.Lerp(-80, 0, masterVolumeValue));
        mixer.audioMixer.SetFloat("MusicVolume", Mathf.Lerp(-80, 0, musicVolumeValue));
        mixer.audioMixer.SetFloat("SoundVolume", Mathf.Lerp(-80, 0, soundVolumeValue));
    }
    public void SetSliderValue()
    {
        master.value = masterVolumeValue;
        music.value = musicVolumeValue;
        sound.value = soundVolumeValue;
    }
}
