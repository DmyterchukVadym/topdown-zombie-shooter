﻿using UnityEngine;
using UnityEngine.UI;

public class Menu_Custom_Set_Game : MonoBehaviour
{
    public Slider sliderCountRoom;
    public Text textCountRoom;

    public void CountRoomSet()
    {
        textCountRoom.text = sliderCountRoom.value.ToString();
    }
}
