﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie_Controller : MonoBehaviour
{
    public float speed = 0.5f;
    public float search_distance = 5;
    public int hp = 100;
    public int attackDamage;
    public float attackDelay = 1;
    [SerializeField] private Transform body;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private Sprite[] skin;
    [SerializeField] private GameObject blood;
    [SerializeField] private GameObject[] ammo;
    [SerializeField] private LayerMask player_layerMask;
    [SerializeField] private LayerMask friend_layerMask;
    private Rigidbody2D rb;
    private Collider2D player;
    private AudioSource voice;
    [SerializeField] AudioClip attackSound;
    [SerializeField] AudioClip damageSound;
    [SerializeField] AudioClip[] idleSounds;
    [SerializeField] AudioClip followSound;

    private bool attack = true;
    public bool go = false;
    private bool isFind = false;
    private bool idle = true;
    private bool idleSpeak = true;

    private Coroutine coroutine_idle = null;
    private Coroutine coroutine_idleSound = null;
    private Coroutine coroutine_idleTime = null;

    public bool Go { set => go = value; }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        voice = GetComponent<AudioSource>();
        //sprite.sprite = skin[Random.Range(0, skin.Length)];
    }

    void Update()
    {
        if (go)
        {
            if (!isFind) player = Physics2D.OverlapCircle(transform.position, search_distance, player_layerMask);
            if (player)
            {
                if (idleSpeak)
                {
                    if (coroutine_idle != null) StopCoroutine(coroutine_idle);
                    if (coroutine_idleSound != null) StopCoroutine(coroutine_idleSound);
                    if (coroutine_idleTime != null) StopCoroutine(coroutine_idleTime);
                    StartCoroutine(FollowVoice());
                }
                idleSpeak = false;
                isFind = true;
                Vector3 temp = player.transform.position - transform.position;
                rb.velocity = (temp) * speed;
                var angle = Vector2.Angle(Vector2.right, rb.velocity);
                if (temp.y < Vector2.right.y) angle *= -1;
                body.transform.eulerAngles = new Vector3(0f, 0f, angle);
            }
            else
            {
                if (idle)
                {
                    coroutine_idle = StartCoroutine(Idle());
                    coroutine_idleSound = StartCoroutine(IdleVoice());
                    idle = false;
                }
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            if (attack)
            {
                Player_Controller player = collision.transform.GetComponent<Player_Controller>();
                player.Damage(attackDamage);
                voice.PlayOneShot(attackSound);
                player.StartCoroutine(player.Force(body.TransformDirection(Vector3.right) * 150));
                attack = false;
                StartCoroutine(AttackDelay());
            }
        }
    }

    private void OnBecameVisible()
    {
        go = true;
    }

    private IEnumerator AttackDelay()
    {
        yield return new WaitForSeconds(attackDelay);
        attack = true;
    }

    public void Damage(int damage)
    {
        hp -= damage;
        voice.PlayOneShot(damageSound);
        go = true;
        search_distance = 20;
        StartCoroutine(GetDamage());
        SpeakToFriends();
        if (hp <= 0)
        {
            FindObjectOfType<Task_Manager>().ZombieWasKilled();
            Instantiate(blood, transform.position, Quaternion.identity);
            if (Random.Range(0, 10) >= 6)
            {
                int luck = Random.Range(0, 10);
                if (luck < 5) Instantiate(ammo[1], transform.position, Quaternion.identity);
                else if (luck < 9) Instantiate(ammo[0], transform.position, Quaternion.identity);
                else if (luck < 10) Instantiate(ammo[2], transform.position, Quaternion.identity);
            }
            Destroy(gameObject);
        }
    }

    void SpeakToFriends()
    {
        Collider2D[] friends = Physics2D.OverlapCircleAll(transform.position, 3, friend_layerMask);
        if (friends.Length != 0)
        {
            foreach (var item in friends)
            {
                item.GetComponent<Zombie_Controller>().search_distance = 30;
                item.GetComponent<Zombie_Controller>().go = true;
            }
        }
    }

    private IEnumerator GetDamage()
    {
        sprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        sprite.color = Color.white;
    }

    private IEnumerator Idle()
    {
        bool idle_go = true;
        Vector3 nextPos = Vector3.zero;
        bool idle_time = true;

        while (true)
        {
            if (idle_go)
            {
                float x = transform.position.x;
                float y = transform.position.y;
                nextPos = new Vector3(Random.Range(x - 3, x + 3.1f), Random.Range(y - 3, y + 3.1f), 0);
                while (Vector3.Distance(nextPos, transform.position) < 1.5)
                {
                    nextPos = new Vector3(Random.Range(x - 3, x + 3.1f), Random.Range(y - 3, y + 3.1f), 0);
                }
                idle_go = false;
            }

            if (idle_time)
            {
                coroutine_idleTime = StartCoroutine(IdleTime());
                idle_time = false;
            }

            Vector3 temp = nextPos - transform.position;
            rb.velocity = (temp) * speed;

            var angle = Vector2.Angle(Vector2.right, rb.velocity);
            if (temp.y < Vector2.right.y) angle *= -1;
            body.transform.eulerAngles = new Vector3(0f, 0f, angle);

            if (Vector3.Distance(nextPos, transform.position) < 1f)
            {
                StopCoroutine(coroutine_idleTime);
                rb.velocity = Vector3.zero;
                yield return new WaitForSeconds(Random.Range(1f, 2f));
                idle_go = true;
                idle_time = true;
            }
            yield return null;
        }
    }

    IEnumerator FollowVoice()
    {
        yield return new WaitForSeconds(Random.Range(4f, 10f));
        if (Random.Range(0, 10) >= 8)
            voice.PlayOneShot(followSound);
    }

    IEnumerator IdleVoice()
    {
        yield return new WaitForSeconds(Random.Range(4f, 10f));
        if (Random.Range(0, 10) >= 8)
            voice.PlayOneShot(idleSounds[Random.Range(0, idleSounds.Length)]);
    }

    IEnumerator IdleTime()
    {
        yield return new WaitForSeconds(2);
        StopCoroutine(coroutine_idle);
        rb.velocity = Vector3.zero;
        yield return new WaitForSeconds(Random.Range(1f, 2f));
        coroutine_idle = StartCoroutine(Idle());
    }
}
