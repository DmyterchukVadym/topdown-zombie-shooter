﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Fast_Icon_Controller : MonoBehaviour
{
    public AudioMixerGroup mixer;
    private float masterVolumeValue;
    private float musicVolumeValue;
    private float tempMasterVolume = 0;
    private float tempMusicVolume = 0;

    public void TotalChangeVolume()
    {
        mixer.audioMixer.GetFloat("MasterVolume", out masterVolumeValue);
        if (masterVolumeValue > -80)
        {
            tempMasterVolume = masterVolumeValue;
            mixer.audioMixer.SetFloat("MasterVolume", -80);
        }
        else
        {
            mixer.audioMixer.SetFloat("MasterVolume", tempMasterVolume);
        }
    }
    public void TextBlick()
    {
        Text temp = GetComponentInChildren<Text>();
        StartCoroutine(CoroutineTextSave(temp));
    }
    IEnumerator CoroutineTextSave(Text temp)
    {
        for (int i = 0; i < 5; i++)
        {
            temp.enabled = !temp.enabled;
            yield return new WaitForSeconds(0.3f);
        }
        temp.enabled = false;
    }
    
    public void PauseInGame()
    {
        mixer.audioMixer.GetFloat("MusicVolume", out musicVolumeValue);
        if (musicVolumeValue > -80)
        {
            tempMusicVolume = musicVolumeValue;
            mixer.audioMixer.SetFloat("MusicVolume", -80);
        }
        else
        {
            mixer.audioMixer.SetFloat("MusicVolume", tempMusicVolume);
        }
        if (Time.timeScale > 0)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
