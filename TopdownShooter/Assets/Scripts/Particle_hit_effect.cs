﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle_hit_effect : MonoBehaviour
{
    public int damage;
    public int power;
    [SerializeField] private GameObject hit_effect;
    ParticleSystem ps;
    List<ParticleCollisionEvent> collEvent = new List<ParticleCollisionEvent>();

    void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    private void OnParticleCollision(GameObject other)
    {

        ps.GetCollisionEvents(other, collEvent);

        switch (other.tag)
        {
            case "NPC":
                //other.GetComponent<Damage_Event>().SetDamage.Invoke(damage);
                other.GetComponent<Zombie_Controller>().Damage(damage);
                Rigidbody2D rb = other.GetComponent<Rigidbody2D>();
                foreach (var item in collEvent)
                {
                    rb.AddForce(item.velocity * power);
                }
                break;
            default:
                foreach (var item in collEvent)
                {
                    Instantiate(hit_effect, item.intersection, Quaternion.identity, transform);
                }
                break;
        }

    }
}
