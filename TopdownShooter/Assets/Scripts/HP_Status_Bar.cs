﻿using UnityEngine;
using UnityEngine.UI;

public class HP_Status_Bar : MonoBehaviour
{
    [SerializeField] Image hp_icon;
    [SerializeField] Text hp_text;
    private int maxHP = 100;
    public int MaxHP { set => maxHP = value; }

    public void SetHP(int hp)
    {
        if (hp >= 0) hp_text.text = hp.ToString();
        else hp = 0;
        hp_icon.fillAmount = (float)hp / (float)maxHP;
    }
}
