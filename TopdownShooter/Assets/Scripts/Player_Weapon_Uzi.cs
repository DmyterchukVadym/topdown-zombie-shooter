﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Weapon_Uzi : MonoBehaviour
{
    private bool isShoot = true;
    [SerializeField] private float attackDelay = 0.1f;
    [SerializeField] private ParticleSystem bullet;
    [SerializeField] private ParticleSystem flash;

    private AudioSource audioSourceShot;
    [SerializeField] private AudioClip uziShot;
    [SerializeField] private AudioClip noAmmo;

    private Player_Weapon_Manager pwm;

    private void OnEnable()
    {
        isShoot = true;
        audioSourceShot = transform.parent.gameObject.GetComponent<AudioSource>();
        pwm = transform.parent.gameObject.GetComponent<Player_Weapon_Manager>();
    }

    void Update()
    {
        if (Input.GetMouseButton(0) && isShoot)
        {
            var mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            mousePosition = new Vector3(mousePosition.x, mousePosition.y, 0);
            if (Vector3.Distance(mousePosition, bullet.transform.position) > 3)
            {
                var angle = Vector2.Angle(Vector2.up, mousePosition - bullet.transform.position);
                bullet.transform.eulerAngles = new Vector3(0f, 0f, bullet.transform.position.x > mousePosition.x ? angle : -angle);
            }
            else
            {
                bullet.transform.localEulerAngles = new Vector3(0f, 0f, -90f);
            }
            if (pwm.AmmoUzi > 0)
            {
                bullet.Emit(1);
                flash.Emit(1);
                audioSourceShot.PlayOneShot(uziShot);
                pwm.AmmoUzi = 1;
            }
            else audioSourceShot.PlayOneShot(noAmmo);
            isShoot = false;
            StartCoroutine(ShootDelay());
        }
    }

    IEnumerator ShootDelay()
    {
        yield return new WaitForSeconds(attackDelay);
        isShoot = true;
    }
}
