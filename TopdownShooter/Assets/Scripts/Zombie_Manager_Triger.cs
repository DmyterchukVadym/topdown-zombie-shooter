﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Zombie_Manager_Triger : MonoBehaviour
{
    [SerializeField] GameObject zombie;
    Transform[] spawnpoints;
    List<Transform> list;
    private int zombieCounter = 0;
    private const int maxZombie = 20;
    [SerializeField] private LayerMask barrier;
    private bool go = false;

    void Start()
    {
        spawnpoints = transform.parent.gameObject.GetComponentsInChildren<Transform>().Where(t => t.CompareTag("SpawnPoint")).ToArray();
        list = new List<Transform>(spawnpoints);
        SpawnZombie(10);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Zombie_Controller[] zombies = GetComponentsInChildren<Zombie_Controller>();
            foreach (var item in zombies) item.Go = true;
            go = true;
            StartCoroutine(ZombieTriger());
        }
    }

    void SpawnZombie(int countOfZombie)
    {
        for (int i = 0; i < countOfZombie; i++)
        {
            if (zombieCounter < maxZombie)
            {
                if (list.Count == 0) list = new List<Transform>(spawnpoints);
                Transform temp = list[Random.Range(0, list.Count)];
                if (!Physics2D.OverlapCircle(transform.position, 2, barrier))
                {
                    GameObject obj = Instantiate(zombie, temp.position, Quaternion.identity, transform);
                    obj.GetComponent<Zombie_Controller>().Go = go;
                    list.Remove(temp);
                    zombieCounter++; ;
                }
            }
        }
    }

    IEnumerator ZombieTriger()
    {
        while (zombieCounter < maxZombie)
        {
            yield return new WaitForSeconds(Random.Range(1, 2));
            SpawnZombie(1);
        }
    }
}
