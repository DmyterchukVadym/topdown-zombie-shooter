﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Task_Manager : MonoBehaviour
{
    private int numberOfRoom;
    private int numberOfZombies;
    [SerializeField] private Text task;

    private bool isActive = false;

    public void Reset() => isActive = false;

    private void Start()
    {
        numberOfRoom = FindObjectOfType<Map_Generator>().NumberOfRooms;
        numberOfZombies = numberOfRoom * 20;
        //task.text = " Зачистіть будинок";
    }

    public void ZombieWasKilled()
    {
        numberOfZombies--;
        if (numberOfZombies <= 0 && !isActive)
        {
            FindObjectOfType<Triger_Final>().Exit = true;
            isActive = true;
            //task.text = "Прямуйте до машини";
        }
    }
}
