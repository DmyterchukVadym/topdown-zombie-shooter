﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triger_Final : MonoBehaviour
{
    private bool exit = false;
    public bool Exit { set => exit = value; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && exit)
        {
            FindObjectOfType<Canvas_In_Game>().ActiveMissionCompleted();
        }
    }
}
