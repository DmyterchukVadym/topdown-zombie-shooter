﻿using UnityEngine;
using UnityEngine.Audio;

public class Music_Manager : MonoBehaviour
{
    public AudioMixerGroup mixer;
    [SerializeField] private AudioSource backgroundAudioSource;
    [SerializeField] private AudioClip[] backgroundMusics;
    [SerializeField] private AudioClip loading;
    [SerializeField] private AudioClip[] gameOver;
    [SerializeField] private AudioClip missionCompleted;

    private void Start()
    {
        backgroundAudioSource.PlayOneShot(backgroundMusics[0]);
        
    }
    private void FixedUpdate()
    {
        if (!backgroundAudioSource.isPlaying)
        {
            backgroundAudioSource.PlayOneShot(backgroundMusics[Random.Range(0, backgroundMusics.Length)]);
        }
    }
    public void LoadingPlayMusic()
    {
        backgroundAudioSource.Stop();
        backgroundAudioSource.PlayOneShot(loading);
    }
    public void MenuGamePlayMusic()
    {
        backgroundAudioSource.Stop();
        backgroundAudioSource.PlayOneShot(backgroundMusics[Random.Range(0, backgroundMusics.Length)]);
    }
    public void MissionCompletedPlayMusic()
    {
        backgroundAudioSource.Stop();
        backgroundAudioSource.PlayOneShot(missionCompleted);
    }
    public void GameOverPlayMusic()
    {
        backgroundAudioSource.Stop();
        backgroundAudioSource.PlayOneShot(gameOver[Random.Range(0, gameOver.Length)]);
    }
}
