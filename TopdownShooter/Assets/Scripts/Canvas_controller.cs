﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Canvas_controller : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Canvas canvasMainMenu;
    [SerializeField] private Canvas canvasInGame;
    [SerializeField] private Canvas canvasLoadImage;

    [SerializeField] private GameObject gridMenu;
    [SerializeField] private GameObject cursor;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject task_manager;

    private GameObject tempGridMenu;
    private Map_Generator map_Generator;
    private Music_Manager musicManager;
    private Canvas_Loading_Game canvas_Loading_Game;
    private Canvas_In_Game canvas_In_Game;

    private bool isMenu = false;
    private bool isDone = false;
    public bool IsDone { set => isDone = value; }

    void Start()
    {
        musicManager = FindObjectOfType<Music_Manager>();
        map_Generator = FindObjectOfType<Map_Generator>();
        canvas_Loading_Game = FindObjectOfType<Canvas_Loading_Game>();
        canvas_In_Game = FindObjectOfType<Canvas_In_Game>();

        tempGridMenu = Instantiate(gridMenu, Vector3.zero, Quaternion.identity, transform);
    }
    public void InGame(Map_Generator.StartPos startPos, int numberOfRooms, bool corridor, int rows, int columns, int roomsOnRow, int roomsOnColumn)
    {
        canvasLoadImage.GetComponent<Canvas>().enabled = !canvasLoadImage.GetComponent<Canvas>().enabled;
        canvas_Loading_Game.LogoBlick();
        Instantiate(task_manager, Vector3.zero, Quaternion.identity);
        map_Generator.Clear();
        map_Generator.SetStartParams(startPos, numberOfRooms, corridor, rows, columns, roomsOnRow, roomsOnColumn);
        map_Generator.GenerateMap();
        Instantiate(cursor, new Vector3(0, 1, 0), Quaternion.identity);
        Instantiate(player, Vector3.zero, Quaternion.identity);
        mainCamera.GetComponent<Camera_Move>().Reset();
        StartCoroutine(LoadGame());
    }
    public void InMenu()
    {
        canvasLoadImage.GetComponent<Canvas>().enabled = !canvasLoadImage.GetComponent<Canvas>().enabled;
        canvas_Loading_Game.LogoBlick();

        map_Generator.Clear();
        try { Destroy(FindObjectOfType<Player_Controller>().gameObject); } catch { }
        Destroy(FindObjectOfType<Task_Manager>().gameObject);
        StartCoroutine(LoadGame());
    }

    private IEnumerator LoadGame()
    {
        musicManager.LoadingPlayMusic();
        mainCamera.GetComponent<Menu_Camera_Move>().enabled = !mainCamera.GetComponent<Menu_Camera_Move>().enabled;
        mainCamera.GetComponent<Camera_Move>().enabled = !mainCamera.GetComponent<Camera_Move>().enabled;
        canvasMainMenu.GetComponent<Canvas>().enabled = !canvasMainMenu.GetComponent<Canvas>().enabled;
        canvasInGame.GetComponent<Canvas>().enabled = !canvasInGame.GetComponent<Canvas>().enabled;
        if (!isMenu)
        {
            canvas_In_Game.EnabledPanelInGame();
            Destroy(tempGridMenu);
        }
        else
        {
            canvas_In_Game.DisabledPanelInGame();
            tempGridMenu = Instantiate(gridMenu, Vector3.zero, Quaternion.identity, transform);
        }
        isMenu = !isMenu;
        yield return new WaitForSeconds(2);
        while (!isDone) { }
        canvasLoadImage.GetComponent<Canvas>().enabled = !canvasLoadImage.GetComponent<Canvas>().enabled;
        musicManager.MenuGamePlayMusic();
    }
}
